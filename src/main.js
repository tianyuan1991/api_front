// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import util from './assets/util' // Set Globle URL Path
import router from './router'
import Vuetify from 'vuetify'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueCookie from 'vue-cookie'

import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)
Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(util)
Vue.config.productionTip = false
Vue.use(VueCookie)

// 需要维护的状态
const store = new Vuex.Store({
  state: {
    username: '',
    token: ''
  },
  mutations: {
    setuser (state, data) {
      state.username = data.username
      state.token = data.token
    },
    removeuser (state) {
      state.username = ''
      state.token = ''
    }
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
