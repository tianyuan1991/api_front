module.exports = {
  register: function (url, username, password) {
    return "import requests\nimport json\n\nurl = '" + url + "'\n\ndata = {\n 'username': '" + username + "',\n 'password': '" + password + "'\n}\n\nr = requests.post(url, json=data)\n"
  },
  login: function (url, username, password) {
    return "import requests\nimport json\n\nurl = '" + url + "'\n\ndata = {\n 'username': '" + username + "',\n 'password': '" + password + "'\n}\n\nr = requests.post(url, json=data)\n"
  },
  getuserinfo: function (url, token) {
    return "import requests\n\nurl = '" + url + "'\n\nheaders = {'Authorization': '" + token + "'}\n\nr = requests.get(url,headers=headers)\n"
  },
  setrole: function (url, token) {
    return "import requests\n\nurl = '" + url + "'\n\nheaders = {'Authorization': '" + token + "'}\ndata = {\n 'user_id': 'XXXX',\n}\n\nr = requests.post(url,headers=headers, json=data)"
  },
  removeuser: function (url, token) {
    return "import requests\n\nurl = '" + url + "'\n\nheaders = {'Authorization': '" + token + "'}\ndata = {\n 'user_id': 'XXXX',\n}\n\nr = requests.post(url,headers=headers, json=data)"
  },
  getendpointlist: function (url, token) {
    return "import requests\n\nurl = '" + url + "'\n\nheaders = {'Authorization': '" + token + "'}\n\nr = requests.get(url,headers=headers)\n"
  },
  createendpoint: function (url, token, endpointname, endpointdesc, collectionlist) {
    return "import requests\r\n\r\nurl = '" + url + "'\r\n\r\nheaders = {'Authorization': '" + token + "'}\r\ndata = {\r\n 'endpoint_name': '" + endpointname + "',\r\n 'endpoint_desc': '" + endpointdesc + "',\r\n 'collection_list': '" + collectionlist + "'\r\n}\r\n\r\nr = requests.post(url,headers=headers, json=data)"
  },
  removeendpoint: function (url, token, endpointid) {
    return "import requests\r\n\r\nurl = '" + url + "'\r\n\r\nheaders = {'Authorization': '" + token + "'}\r\ndata = {\r\n 'endpoint_id': '" + endpointid + "',\r\n}\r\n\r\nr = requests.post(url,headers=headers, json=data)"
  },
  getcollectionlist: function (url, token) {
    return "import requests\r\n\r\nurl = '" + url + "'\r\n\r\nheaders = {'Authorization': '" + token + "'}\r\n\r\nr = requests.get(url,headers=headers)"
  },
  createcollection: function (url, token) {
    return "import requests\r\n\r\nurl = '" + url + "'\r\n\r\nheaders = {'Authorization': '" + token + "'}\r\ndata = {\r\n 'collection_name': 'Test_Collection',\r\n 'collection_des': 'This is a test collection...'\r\n}\r\n\r\nr = requests.post(url,headers=headers, json=data)"
  },
  removecollection: function (url, token) {
    return "import requests\r\n\r\nurl = '" + url + "'\r\n\r\nheaders = {'Authorization': '" + token + "'}\r\ndata = {\r\n 'collection_id': '',\r\n}\r\n\r\nr = requests.post(url,headers=headers, json=data)"
  },
  insertdata: function (url, token) {
    return "import requests\r\nimport json\r\n\r\nurl = '" + url + "'\r\n\nheaders = {'Authorization': '" + token + "'}\r\n\n# 我在这里读取了JSON文件，但其实可以用任何方式获取数据，只需要确保格式一致就行。\r\ndata = []\r\nwith open('test.json', encoding='utf-8', mode = 'r') as f:\r\n for line in f:\r\n data.append(json.loads(line))\r\n\r\nr = requests.post(url,headers=headers, json=data)"
  },
  fetchdata: function (url, token) {
    return "import requests\r\nimport json\r\n\r\nurl = '" + url + "'\r\nheaders = {'Authorization': '" + token + "'}\r\n\r\npara = {\r\n 'limit': 3,\r\n 'offset': 5\r\n }\r\n\r\nr = requests.get(url,headers=headers, params=para)\r\noutput = json.loads(r.text)"
  },
  fetchquery: function (url, token, query) {
    return "import requests\r\nimport json\r\n\r\nurl = '" + url + "'\r\nheaders = {'Authorization': '" + token + "'}\r\n\r\ndata = " + query + '\r\n\r\nr = requests.post(url,headers=headers, json=data)\r\n\r\noutput = json.loads(r.text)'
  }
}
