# Frontend

> Front pages for DataFusion plateform, which is designed by Yuan Tian

This is the Frontend of Banyan project, which could be seen as interface and visualization tool for users. Honestly compared with more other IT companies platform, this frontend of way complicated.

## Purpose of Frontend for Banyan

The frontend need to perfectly show graphs on data transmitted from backend API. Thus the match between backend and frontend must be perfectly good. Also, Users need to be able to mulnipulate their data easily, by simply selecting dimensions or key words. We need to find a way to bridging gap between thousands of clients and thousands of data sets, which is never a easy task.

 - Pages of company
 - Visualization of API data with highcharts.
 - Interactive conductioin between data and webpage.
 - Robust enough

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

