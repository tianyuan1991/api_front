import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/HelloWorld',
      name: 'HelloWorld',
      component: resolve => require(['@/components/HelloWorld'], resolve)
    },
    {
      path: '/',
      name: 'Home',
      component: resolve => require(['@/components/Home'], resolve)
    },
    {
      path: '/user',
      name: 'User',
      component: resolve => require(['@/components/User'], resolve)
    },
    {
      path: '/endpoints',
      name: 'EndPoints',
      component: resolve => require(['@/components/EndPoints'], resolve)
    },
    {
      path: '/apis',
      name: 'APIs',
      component: resolve => require(['@/components/APIs'], resolve)
    },
    {
      path: '/endpoint/:EndpointName',
      name: 'EndPoint',
      component: resolve => require(['@/components/EndPointList'], resolve)
    },
    {
      path: '/:EndpointName/:CollectionID/get',
      name: 'GetCollectionData',
      component: resolve => require(['@/components/GetCollectionData'], resolve)
    },
    {
      path: '/:EndpointName/:CollectionID/add',
      name: 'PostCollectionData',
      component: resolve => require(['@/components/PostColletionData'], resolve)
    },
    {
      path: '/:EndpointName/:CollectionID/create_api',
      name: 'CreateAPI',
      component: resolve => require(['@/components/CreateAPI'], resolve)
    }
  ]
})
